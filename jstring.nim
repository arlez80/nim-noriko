#[
	noriko/jstring - 日本語文字列処理
		by あるる（きのもと 結衣）
]#

import unicode

proc isHiragana*( s:string ): bool =
    let code = s.runeAtPos(0)
    return Rune(0x3041) <=% code and code <=% Rune(0x3096)

proc isKatakana*( s:string ): bool =
    let code = s.runeAtPos(0)
    return Rune(0x30A1) <=% code and code <=% Rune(0x30FA)

proc isHalfKatakana*( s:string ): bool =
    let code = s.runeAtPos(0)
    return Rune(0xFF66) <=% code and code <=% Rune(0xFF9D)

proc isKanji*( s:string ): bool =
    let code = s.runeAtPos(0)
    return (
        # CJK統合漢字拡張A
        ( Rune(0x3400) <=% code and code <=% Rune(0x4DBF) ) or
        # CJK統合漢字
        ( Rune(0x4E00) <=% code and code <=% Rune(0x9FFF) ) or
        # CJK互換漢字
        ( Rune(0xF900) <=% code and code <=% Rune(0xFA99) )
    )
